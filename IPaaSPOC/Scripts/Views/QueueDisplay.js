﻿function QueueView() {
    _ = this;
    var queueCount = 0;
    this.load = function () {
        _.loadQueueDisplay();
        $(document).on("click", "#add2QButton", function () { 
            for (var i = 0; i < 10000; i++) {
                var data = getNewData()
                $.ajax({
                    url: siteRoot() + 'api/queue/add',
                    type: "POST",
                    data: data,
                    success: function (data) {
                        //_.renderMustacheTableData(data.Item);
                        //$('#QueueDisplay').fadeTo(250, 0, "swing", () => {
                        //    _.renderMustacheTableData(data.Item);
                        //});
                        //_.loadQueueDisplay();
                        queueCount += 1;
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var data = {};
                        data.errorType = 'businesslogic';
                        data.errorMessage = "There was an AJAX Form Submission Error (" + jqXHR.status + "): " + errorThrown;
                        OnAjaxReturnedError(data);
                    }
                });
            }
        });
        $(document).on("click", "#deleteButton", function () {
            $.ajax({
                url: siteRoot() + 'api/queue/deleteAll',
                type: "GET",
                success: function (data) {
                    _.loadQueueDisplay();
                    queueCount = 0;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    var data = {};
                    data.errorType = 'businesslogic';
                    data.errorMessage = "There was an AJAX Form Submission Error (" + jqXHR.status + "): " + errorThrown;
                    OnAjaxReturnedError(data);
                }
            })
        })
    };

    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
    function getRandomIntWithSpecifiedLength(length) {
        var randomInt = "";
        for (var digitPos = 0; digitPos < length; digitPos++) {
            randomInt += getRandomInt(0, 9);
        }
        return randomInt;
    }
    function getRandomLetter() {
        var letterIndex = getRandomInt(0, 25);
        var letters = "abcdefghijklmnopqrstuvwxyz";
        return letters[letterIndex];
    }
    function getNewData() {
        var custNumValue = getRandomInt(0, 99999);
        var custIDValue = "" + getRandomLetter() + getRandomLetter() + getRandomIntWithSpecifiedLength(4); 
        return {
            "custNum": custNumValue,
            "custID": custIDValue
        }
    }


    this.renderMustacheTableData = function (data) {
        var strQueueDisplayTemplateHtml = $('#QueueDisplayTemplate').html();

        var options = {
            year: 'numeric',
            month: 'short',
            day: '2-digit',
            hour: 'numeric',
            minute: 'numeric',
            second: 'numeric'
        };

        if (data.CreatedDate != null) {
            var _resultCreatedDate = new Intl.DateTimeFormat('en-us', options).format(data.CreateDate);
            data.CreatedDate = _resultCreatedDate;
        }

        if (data.ProcessedDate != null) {
            var _resultProcessedDate = new Intl.DateTimeFormat('en-us', options).format(data.ProcessedDate);
            data.ProcessedDate = _resultProcessedDate;
        }

        if (data.ProcessedDate == null) {
            data.ProcessedDate = "null";
        }

        if (data.Status == null) {
            data.Status = "null";
        }

        var strRenderedQueueDisplay = Mustache.to_html(strQueueDisplayTemplateHtml, data);
        $('#QueueDisplay').html(strRenderedQueueDisplay);
        $('#QueueDisplay').fadeTo(250, 1);
    }

    this.loadQueueDisplay = function () {
        $.ajax({
            url: siteRoot() + 'api/queue',
            type: "GET",
            success: function (data) {
                $('#QueueDisplay').fadeTo(250, 0, "swing", () =>
                {
                    _.renderMustacheTableData(data.Items);
                    queueCount = data.Items.length;
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                var data = {};
                data.errorType = 'businesslogic';
                data.errorMessage = "There was an AJAX Form Submission Error (" + jqXHR.status + "): " + errorThrown;
                OnAjaxReturnedError(data);
            }
        })
    }
    function OnAjaxReturnedError(data){
        if (data.success == true) {
            // this function must have been accidentally called
            return;
        }

        if (data.errorType.toLowerCase() == "login") {
            if ($('#saving-dialog')) {
                $('#saving-dialog').remove();
            }
            displayAlert(data.errorMessage);
            return;
        }

        if (data.errorType.toLowerCase() == "businesslogic") {
            if (window.opener != null && window.opener.jQuery('#saving-dialog')) {
                window.opener.jQuery('#saving-dialog').remove();
            }
            if ($('#saving-dialog')) {
                $('#saving-dialog').remove();
                $('#createcallbtn, #closepopup').prop('disabled', false);
            }
            displayAlert(data.errorMessage);
            return;
        }
    }
    function displayAlert(strAlertMessage) {

        var j$modalDialog = $('<div></div>')
         .html(strAlertMessage)
         .dialog({
             autoOpen: false,
             title: 'Alert',
             resizable: false,
             width: 400,
             height: 100,
             autoResize: true,
             modal: true,
             draggable: true,
             dialogClass: 'no-close alert-dialog'
         });

        j$modalDialog.dialog('open');

    }

    function PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }
}

var _ = new QueueView();

window.onload = function () {
    $(_.load());
};
