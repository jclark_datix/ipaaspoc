﻿using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.Documents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPaaSPOC.Models
{
	public class CustomerQueueEntity : QueueEntity
	{
		public CustomerQueueEntity()
		{
			PartitionKey = "EpicorCustomersSFDCQueue";
			RowKey = Guid.NewGuid().ToString();
		}

		public string CustNum { get; set; }
		public string CustID { get; set; }
	}
}
