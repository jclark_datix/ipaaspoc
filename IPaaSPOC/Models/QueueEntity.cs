﻿using Microsoft.Azure.Cosmos.Table;
using Microsoft.Azure.Documents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPaaSPOC.Models
{
	public class QueueEntity : TableEntity
	{
		public QueueEntity()
		{
		}

		public QueueEntity(string queueName, string rowID)
		{
			PartitionKey = queueName;
			RowKey = rowID;
		}

		public int BatchStep { get; set; }
		public int Priority { get; set; }
		public bool Processing { get; set; }
		public string Status { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public DateTime? CreatedDate { get; set; }

		[JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
		public DateTime? ProcessedDate { get; set; }
	}
}