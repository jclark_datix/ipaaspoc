﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using Microsoft.Azure.Cosmos.Table;
using System.Security.AccessControl;
using System.Configuration;
using IPaaSPOC.Models;
using System.Web.Management;

namespace IPaaSPOC.Controllers
{
    [Authorize]
    public class QueueController : ApiController
    {
        // GET api/queue/
        public Object GetUnprocessedItems()
        {
            string storageConnectionString = ConfigurationManager.ConnectionStrings["DatixAzureTableStorageConnectionString"].ConnectionString;
            CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(storageConnectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable table = tableClient.GetTableReference("EpicorCustomersSFDCQueue");

            string batchStepFilter = TableQuery.GenerateFilterConditionForInt("BatchStep", QueryComparisons.Equal, 1);
            string processingFilter = TableQuery.GenerateFilterConditionForBool ("Processing", QueryComparisons.Equal, false);

            string nextItemFilter = TableQuery.CombineFilters(
                batchStepFilter,
                TableOperators.And,
                processingFilter);
            List<CustomerQueueEntity> lstUnprocessedItems;
            try
            {
                TableQuery<CustomerQueueEntity> nextItemQuery = new TableQuery<CustomerQueueEntity>()
                    .Where(nextItemFilter);

                IEnumerable<CustomerQueueEntity> items = table.ExecuteQuery(nextItemQuery);
                List<CustomerQueueEntity> lstItems = items.ToList();
                IEnumerable<CustomerQueueEntity> unprocessedItems = items
                    .Where(item => item.ProcessedDate == null)
                    .OrderBy(item => item.Priority)
                    .ThenBy(item => item.CreatedDate);
                lstUnprocessedItems = unprocessedItems.ToList();
            }
            catch (StorageException e)
            {
                return new
                {
                    Success = false,
                    Message = e.Message
                };
            }
            return new
            {
                Success = true,
                Items = lstUnprocessedItems
            };
        }

        [Route("api/queue/add")]
        public Object AddtoQueue(CustomerQueueEntity customerQueueEntity)
        {
            string storageConnectionString = ConfigurationManager.ConnectionStrings["DatixAzureTableStorageConnectionString"].ConnectionString;
            CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(storageConnectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable table = tableClient.GetTableReference("EpicorCustomersSFDCQueue");

            customerQueueEntity.BatchStep = 1;
            customerQueueEntity.CreatedDate = DateTime.Now;
            customerQueueEntity.Priority = 3;

            try
            {
                table.ExecuteAsync(TableOperation.Insert(customerQueueEntity));
            }
            catch (StorageException e)
            {
                return new
                {
                    Success = false,
                    Message = e.Message
                };
            }
            return new
            {
                Success = true,
                Item = customerQueueEntity
            };
        }

        [Route("api/queue/processItem")]
        public Object ProcessItem(CustomerQueueEntity unprocessedItem)
        {
            string storageConnectionString = ConfigurationManager.ConnectionStrings["DatixAzureTableStorageConnectionString"].ConnectionString;
            CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(storageConnectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable table = tableClient.GetTableReference("EpicorCustomersSFDCQueue");

			unprocessedItem.Status = "completed";
			unprocessedItem.ProcessedDate = DateTime.Now;
            try
            {
                table.Execute(TableOperation.Merge(unprocessedItem));
            } 
            catch (StorageException e) 
            {
                return new
                {
                    Success = false,
                    Message = e.Message
                };
            }

            return new
            {
                Success = true,
                Item = unprocessedItem
            };
        }

        [HttpGet]
        [Route("api/queue/deleteAll")]
        public Object DeleteAllItems()
        {
            string storageConnectionString = ConfigurationManager.ConnectionStrings["DatixAzureTableStorageConnectionString"].ConnectionString;
            CloudStorageAccount storageAccount = Common.CreateStorageAccountFromConnectionString(storageConnectionString);
            CloudTableClient tableClient = storageAccount.CreateCloudTableClient(new TableClientConfiguration());
            CloudTable table = tableClient.GetTableReference("EpicorCustomersSFDCQueue");

            try
            {
				table.Delete();

				CloudTable table2 = tableClient.GetTableReference("EpicorCustomersSFDCQueue");
				table2.CreateIfNotExists();
            } 
            catch (StorageException e) 
            {
                return new
                {
                    Success = false,
                    Message = e.Message
                };
            }

            return new
            {
                Success = true
            };
        }
    }
}
